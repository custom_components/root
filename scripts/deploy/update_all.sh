#!/bin/bash
# Author: Joakim Sørensen @ludeeus

# Set root JSON file
ROOT_JSON=$(curl -sSL https://gitlab.com/custom_components/root/raw/master/root.json)
REPOS=$(echo ${ROOT_JSON} | jq -r 'keys[]')
mkdir /update

git config --global user.name "${USER}"
git config --global user.email "${MAIL}"

function update_license {
    author=$(echo $ROOT_JSON | jq -r .$1.author)
    if [[ "$author" = *"ludeeus"* ]]; then
        echo "Updating LICENSE file"
        yes | cp -rf ${CI_PROJECT_DIR}/LICENSE $2/LICENSE
        author=$(echo $ROOT_JSON | jq -r .$1.author)
        sed -i "3s/.*/Copyright (c) $(TZ="Europe/Oslo" date +"%Y") $author/" $2/LICENSE
    fi
}

function update_ci_config {
    if [ -f $2/.gitlab-ci.yml ]; then
        rm $2/.gitlab-ci.yml
    fi
    echo "Updating CI config"
    printf '%s\n' "update_readme:" | tee -a $2/.gitlab-ci.yml
    printf '%s\n' "  stage: build" | tee -a $2/.gitlab-ci.yml
    printf '%s\n' "  script:" | tee -a $2/.gitlab-ci.yml
    printf '%s\n' "    - curl -X POST -F token="\$""README_TOKEN" -F ref=master -F "variables[single_repo]=$1" https://gitlab.com/api/v4/projects/7064305/trigger/pipeline" | tee -a $2/.gitlab-ci.yml
    author=$(echo $ROOT_JSON | jq -r .$1.author)
    if [[ "$author" = *"ludeeus"* ]]; then
        printf '%s\n' "push_github:" | tee -a $2/.gitlab-ci.yml
        printf '%s\n' "  stage: deploy" | tee -a $2/.gitlab-ci.yml
        printf '%s\n' "  script:" | tee -a $2/.gitlab-ci.yml
        printf '%s\n' "    - curl -sL https://gitlab.com/ludeeus/toolbox/raw/master/CI-Scripts/push_to_public_repo/init.sh | bash -s github HA-Custom_components "root" /$1" | tee -a $2/.gitlab-ci.yml
        printf '%s\n' "    - curl -sL https://gitlab.com/ludeeus/toolbox/raw/master/CI-Scripts/push_to_public_repo/push.sh | bash -s Europe/Oslo" | tee -a $2/.gitlab-ci.yml
    fi
}

function update_readme {
    if [ -f $2/README.md ]; then
        rm $2/README.md
    fi
    if [ -f $2/root.json ]; then
        ROOT_JSON=$(cat "$2"/root.json)
    else
        ROOT_JSON=$(curl -sSL https://gitlab.com/custom_components/root/raw/master/root.json)
    fi
    title=$(echo $ROOT_JSON | jq -r .$1.title)
    mantained=$(echo $ROOT_JSON | jq -r .$1.mantained)
    component=$(echo $ROOT_JSON | jq -r .$1.component)
    author=$(echo $ROOT_JSON | jq -r .$1.author)
    added_to_ha=$(echo $ROOT_JSON | jq -r .$1.added_to_ha)
    added_in_version=$(echo $ROOT_JSON | jq -r .$1.added_in_version)
    overview_img=$(echo $ROOT_JSON | jq -r .$1.overview_img)
    short_description=$(echo $ROOT_JSON | jq -r .$1.short_description)
    demo_site_available=$(echo $ROOT_JSON | jq -r .$1.demo_site_available)
    demo_site_url=$(echo $ROOT_JSON | jq -r .$1.demo_site_url)
    forumpost=$(echo $ROOT_JSON | jq -r .$1.forumpost)
    instructions=$(echo $ROOT_JSON | jq -r .$1.instructions)
    ext_licenses=$(echo $ROOT_JSON | jq -r .$1.ext_licenses)
    configuration=$(echo $ROOT_JSON | jq -r .$1.configuration)

    versionformfile=$(cat $(find "$2"/custom_components/ -type f -name *.py) | grep '_VERSION =' | tail -1 | awk -F " " '{print $3}')
    if [ -z "$versionformfile" ]; then
        echo "Could not find version in file.."
        version=$(echo $ROOT_JSON | jq -r .$1.version)
    else
        echo "Found version in file.."
        version=${versionformfile:1:-1}
    fi

    maintainer=${author// /%20}
    maintainer=${maintainer//@/%40}

    echo "Creating README.md"
    echo "# $title" | tee -a $2/README.md
    printf '%s\n' "  " | tee -a $2/README.md
    if [ "$added_to_ha" == "true" ]; then
        echo "### This custom_component is from $added_in_version, a part of Home-Assistant." | tee -a $2/README.md
        echo "For updated documentation see:  " | tee -a $2/README.md
        if [ "$component" == "true"  ]; then
            echo "https://www.home-assistant.io/components/$1" | tee -a $2/README.md
        else
            echo "https://www.home-assistant.io/components/$component.$1" | tee -a $2/README.md
        fi
        printf '%s\n' "  " | tee -a $2/README.md
    else
        printf "[![Version](https://img.shields.io/badge/version-$version-green.svg?style=for-the-badge)](#) " | tee -a $2/README.md
        if [ "$mantained" == "yes"  ]; then
            printf "[![mantained](https://img.shields.io/maintenance/$mantained/$(TZ="Europe/Oslo" date +"%Y").svg?style=for-the-badge)](#) " | tee -a $2/README.md
            printf "[![maintainer](https://img.shields.io/badge/maintainer-%s-blue.svg?style=for-the-badge)](#) " "$maintainer" | tee -a $2/README.md
        else
            printf "[![mantained](https://img.shields.io/maintenance/no/$mantained.svg?style=for-the-badge)](#) " | tee -a $2/README.md
        fi
        if [ "$forumpost" != "null" ]; then
            printf "[![forum](https://img.shields.io/badge/forum-visit-orange.svg?style=for-the-badge)]($forumpost) " | tee -a $2/README.md
        fi
        printf '%s\n' "  " | tee -a $2/README.md
        echo "$short_description" | tee -a $2/README.md
        printf '%s\n' "  " | tee -a $2/README.md
        if [ "$component" == "true"  ]; then
            echo "To get started put \`/custom_components/$1.py\`  " | tee -a $2/README.md
            echo "here: \`<config directory>/custom_components/$1.py\`  " | tee -a $2/README.md
        else
            echo "To get started put \`/custom_components/$component/$1.py\` here:  " | tee -a $2/README.md
            echo "\`<config directory>/custom_components/$component/$1.py\`  " | tee -a $2/README.md
        fi
        printf '%s\n' "  " | tee -a $2/README.md
        echo "**Example configuration.yaml:**" | tee -a $2/README.md
        echo "\`\`\`yaml" | tee -a $2/README.md
        if [ "$component" == "true"  ]; then
            echo "$1:" | tee -a $2/README.md
        else
            echo "$component:" | tee -a $2/README.md
        fi
        if [ "$configuration" != "none"  ]; then
            for keys in $(echo $ROOT_JSON | jq -r .$1.configuration | jq -r keys_unsorted[]); do
                sub=$(echo $ROOT_JSON | jq -r .$1.configuration.$keys)
                if [[ $sub = *"["* ]]; then
                    listkey=$(echo $ROOT_JSON | jq -r .$1.configuration.$keys | awk -F'[' '{print $NF}' | awk -F']' '{print $1}')
                    printf '%s\n' "  $keys:" | tee -a $2/README.md
                    for key in $listkey; do
                        printf '%s\n' "    - $key" | tee -a $2/README.md
                    done
                else
                    printf '%s\n' "  $keys: $(echo $ROOT_JSON | jq -r .$1.configuration.$keys)" | tee -a $2/README.md
                fi
            done
            echo "\`\`\`" | tee -a $2/README.md
            echo "**Configuration variables:**  " | tee -a $2/README.md
            printf '%s\n' "  " | tee -a $2/README.md
            echo "key | description  " | tee -a $2/README.md
            echo ":--- | :---  " | tee -a $2/README.md
            for required in $(echo $ROOT_JSON | jq -r .$1.configuration_variables.required | jq -r keys_unsorted[]); do
                echo "**$required (Required)** | $(echo $ROOT_JSON | jq -r .$1.configuration_variables.required.$required)  " | tee -a $2/README.md
            done
            for optional in $(echo $ROOT_JSON | jq -r .$1.configuration_variables.optional | jq -r keys_unsorted[]); do
                echo "**$optional (Optional)** | $(echo $ROOT_JSON | jq -r .$1.configuration_variables.optional.$optional)  " | tee -a $2/README.md
            done
        else
            echo "\`\`\`" | tee -a $2/README.md    
        fi
        printf '%s\n' "  " | tee -a $2/README.md
        if [ "$overview_img" == "true" ]; then
            echo "#### Sample overview" | tee -a $2/README.md
            echo "![Sample overview](overview.png)" | tee -a $2/README.md
            printf '%s\n' "  " | tee -a $2/README.md
        fi
        if [ "$demo_site_available" == "true" ]; then
            echo "[Home-Assistant demo site.]($demo_site_url)" | tee -a $2/README.md
            printf '%s\n' "  " | tee -a $2/README.md
        fi
        printf "$instructions" | tee -a $2/README.md
        printf '%s\n' "  " | tee -a $2/README.md
        printf "$ext_licenses" | tee -a $2/README.md
        printf '%s\n' "  " | tee -a $2/README.md
        printf '%s\n' "***" | tee -a $2/README.md
        printf "Due to how \`custom_componentes\` are loaded, it is normal to see a \`ModuleNotFoundError\` error on first boot after adding this, to resolve it, restart Home-Assistant." | tee -a $2/README.md
    fi
}

if [ ! -z $single_repo ];then
    echo "Doing stuff for ${single_repo}"
    repodir=/update/$single_repo
    mkdir $repodir
    cd $repodir
    git init
    git remote add origin https://${USER}:${PASSWORD}@gitlab.com/custom_components/$single_repo.git
    git fetch --all && git reset --hard origin/master

    # run functions
    update_license $single_repo $repodir
    update_readme $single_repo $repodir
    update_ci_config $single_repo $repodir

    # push to repo
    git add .
    git commit -m "Automatic update $(TZ="Europe/Oslo" date +"%Y.%m.%d %H:%M:%S")"
    git push -u origin master
else
    for repo in $REPOS; do
        if [ "$repo" != "demo" ]; then
            echo "Doing stuff for ${repo}"
            repodir=/update/$repo
            mkdir $repodir
            cd $repodir
            git init
            git remote add origin https://${USER}:${PASSWORD}@gitlab.com/custom_components/$repo.git
            git fetch --all && git reset --hard origin/master

            # run functions
            update_readme $repo $repodir
            update_license $repo $repodir
            update_ci_config $repo $repodir

            # push to repo
            git add .
            git commit -m "Automatic update $(TZ="Europe/Oslo" date +"%Y.%m.%d %H:%M:%S")"
            git push -u origin master
        fi
    done
fi